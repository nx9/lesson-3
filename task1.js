// Task 1
// 1
let num = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
for(let i = 0; i < num.length; i++) {
    if(num[i] % 3 === 0) {
        console.log(num[i] + ' is dev by 3');
    }else if(num[i] % 5 === 0) {
        console.log(num[i] + ' is dev by 5');
    }else if(num[i] % 3 ===0 && num[i] % 5 === 0) {
        console.log(num[i] + ' are both dev by 3 nd 5');
    }else {
        console.log('there is not devisble number for 3 and 5');
    }
}

// 2
let givenNumber = 15
switch(givenNumber % 2) {
    case 0:
        console.log(givenNumber + ' is even');
        break;
    case 1 :
        console.log(givenNumber + ' is odd');
        break;
    default:
        console.log(undefined);
}

// 3
let arr = [2, 1 ,5 ,3 , 4]
arr.sort((a, b) => a - b)
console.log(arr);

// 4
let set = [
    {test: ['a', 'b', 'c', 'd']},
    {test: ['a', 'b', 'c']},
    {test: ['a', 'd']},
    {test: ['a', 'b', 'k', 'e', 'e']},
    ]

function uniqueArr(arr) {
    let result = []
    for(let val of arr) {
        Object.values(val)[0].map((el) => {
            if(!result.includes(el)) {
                result.push(el)
            }
        })
    }
    return result
}
console.log(uniqueArr(set));

// Task 4
function compare(a, b) {
    let first = Object.values(a)
    let second = Object.values(b)

    return first == second
}

const sam = {name: 'az'}
const mam = {name: 'az'}

console.log(compare(sam, mam));
